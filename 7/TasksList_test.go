package task2

import (
	"testing"
	"time"
)

var tasksList TasksList
var person = Person{"John", "Bob", "HR"}

func TestTasksCreate(t *testing.T) {

	tasksList.createTask("Asd", "active", "Asd", person, time.Date(2023, time.Month(5), 23, 1, 1, 1, 1, time.UTC))
	var got = len(tasksList.Tasks)
	var want = 1

	if got != want {
		t.Errorf("got %d want %v\n", got, want)
	}

}

func TestTasksDelete(t *testing.T) {
	tasksList.deleteTask(1)
	got := len(tasksList.Tasks)
	want := 0
	if got != want {
		t.Errorf("got %d want %v\n", got, want)
	}
}

func TestTasksUpdate(t *testing.T) {
	tasksList.createTask("Asd", "active", "Asd", person, time.Date(2023, time.Month(5), 23, 1, 1, 1, 1, time.UTC))
	tasksList.updateTask(1, "Hello", "active", "Asd", time.Date(2023, time.Month(5), 23, 1, 1, 1, 1, time.UTC))

}
