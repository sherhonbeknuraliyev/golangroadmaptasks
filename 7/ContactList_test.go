package task2

import (
	"testing"
)

func TestContacts(t *testing.T) {
	var contactList ContactList
	contactList.create("John", "Bob", "asd@mail.ru", "+9981234324", "Kimdior")
	got := contactList.Contacts[0].id
	want := 1
	if got != want {
		t.Errorf("failed: got %d want %v", got, want)
	}

}
