package task2

import (
	"fmt"
	"time"
)

type Person struct {
	FirstName, LastName, Position string
}
type Task struct {
	ID                     int
	Name, Status, Priority string
	CreatedBy              Person
	CreatedAt, DueDate     time.Time
}
type TasksList struct {
	Tasks       []Task
}

func (tasks *TasksList) createTask(Name string, Status string, Priority string, CreatedBy Person, DueDate time.Time) {
	ID := len(tasks.Tasks) + 1
	tasks.Tasks = append(
		tasks.Tasks,
		Task{
			ID:        ID,
			Name:      Name,
			Status:    Status,
			Priority:  Priority,
			CreatedBy: CreatedBy,
			CreatedAt: time.Now(),
			DueDate:   DueDate,
		},
	)
}
func (tasks *TasksList) updateTask(id int, Name string, Status string, Priority string, DueDate time.Time) {
	if len(tasks.Tasks) > 0 {
		for i, val := range tasks.Tasks {
			if val.ID == id {
				tasks.Tasks[i].Name = Name
				tasks.Tasks[i].Status = Status
				tasks.Tasks[i].Priority = Priority
				tasks.Tasks[i].DueDate = DueDate
				break
			}
		}
	}
}
func (tasks *TasksList) deleteTask(id int) {
	if len(tasks.Tasks) > 0 {
		for i, val := range tasks.Tasks {
			if val.ID == id {
				tasks.Tasks = append(tasks.Tasks[:i], tasks.Tasks[i+1:]...)
			}
		}
	}

}
func (tasks *TasksList) getAll() []Task {
	return tasks.Tasks
}
func (tasks *TasksList) getOne(id int) Task {
	var res Task
	if len(tasks.Tasks) > 0 {
		for _, val := range tasks.Tasks {
			if val.ID == id {
				res = val
			}
		}
	}
	return res
}
func (task *Task) printTask() {
	fmt.Printf(
		"ID: %d | Name: %v | Status %v | Priority: %v | DueDate: %v | CreatedAt: %v | CreatedBy: %v\n",
		task.ID, task.Name, task.Status, task.Priority, task.DueDate.Format("2006-01-02 15:04:05"),
		task.CreatedAt.Format("2006-01-02 15:04:05"), task.CreatedBy.FirstName,
	)
}
func (tasks *TasksList) printTasks() {
	for _, val := range tasks.Tasks {
		val.printTask()
	}

}
