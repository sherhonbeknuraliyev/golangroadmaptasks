package task2

import "fmt"

type Contact struct {
	id                                                int
	FirstName, LastName, Email, PhoneNumber, Position string
}
type ContactList struct {
	CompanyName string
	Contacts    []Contact
}

func (contactList *ContactList) create(FirstName string, LastName string, Email string, PhoneNumber string, Position string) {
	id := len(contactList.Contacts) + 1
	contact := Contact{
		id:          id,
		FirstName:   FirstName,
		LastName:    LastName,
		Email:       Email,
		PhoneNumber: PhoneNumber,
		Position:    Position,
	}
	contactList.Contacts = append(contactList.Contacts, contact)
}
func (contactList *ContactList) deleteContact(id int) {
	if len(contactList.Contacts) > 0 {
		for i, val := range contactList.Contacts {
			if val.id == id {
				contactList.Contacts = append(contactList.Contacts[:i], contactList.Contacts[(i+1):len(contactList.Contacts)]...)
				break
			}
		}
	}

}
func (contactList *ContactList) updateContact(id int, NewFirstName string, NewLastName string, NewEmail string, NewPosition string, NewPhoneNumber string) {
	if len(contactList.Contacts) > 0 {
		for _, val := range contactList.Contacts {
			if val.id == id {
				val.FirstName = NewFirstName
				val.Email = NewEmail
				val.Position = NewPosition
				val.LastName = NewLastName
				val.PhoneNumber = NewPhoneNumber
			}
		}
	}
}

func (contactList *ContactList) printAll() {
	fmt.Println("------Contacts------")
	if len(contactList.Contacts) > 0 {
		for _, val := range contactList.Contacts {
			val.printOne()
		}
	}
}

func (contact *Contact) printOne() {
	fmt.Printf(
		"Id:%d | FirstName: %v | LastName: %v | Email: %v | PhoneNumber : %v\n",
		contact.id, contact.FirstName, contact.LastName, contact.Email, contact.PhoneNumber)

}
