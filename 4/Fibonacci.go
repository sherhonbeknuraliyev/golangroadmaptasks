package main

import (
	"fmt"
)

func main() {
	var num int

	fmt.Printf("N=")
	fmt.Scan(&num)
	var f1, f2, i int = 0, 1, 0

	for i < num {
		fmt.Print(f1, " ")
		t := f2
		f2 = f2 + f1
		f1 = t
		i++
	}

}
