package main

import "fmt"

func main() {
	arr := [8]int64{432, 4324, 543534, 321312, 4325, 3534, 324214, 432}
	fmt.Println(Dup(arr[:]))

}

func Dup(arr []int64) bool {
	k := 0
	for _, s := range arr {

		for _, g := range arr {
			if s == g {

				k++
			}
			if k > 1 {
				return true
			}
		}
		k = 0
	}
	return false
}
