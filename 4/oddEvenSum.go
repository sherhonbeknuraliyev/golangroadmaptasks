package main

import "fmt"

func main() {

	arr := [1000000]int{1, 2, 3, 4, 5, 423, 423, 5345, 654645, 321432, 423, 43243}
	even := 0
	odd := 0
	for i := 0; i < len(arr); i++ {
		if arr[i]%2 == 0 {
			even += arr[i]
		} else {
			odd += arr[i]
		}
	}
	fmt.Println("Even sum: ", even)
	fmt.Println("Odd sum: ", odd)

}
