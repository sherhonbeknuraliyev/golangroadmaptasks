package oop

import (
	"fmt"
	"tasks.com/tasks/7"
)

type Director struct {
	projects task2.TasksList
	teamLeads []TeamLead

}

func (director *Director)giveTask(taskID int, teamLead TeamLead) (error, bool) {
	var res bool = false
	for _, val := range director.teamLeads{
		if val.EqualTo(teamLead){
			res = true
		}

	}
	if res{
		return fmt.Errorf("%v Not found",teamLead.nameOfTeam),false
	}
	for _, val := range director.projects.Tasks{
		if val.ID == taskID {
			teamLead.task = val
		}
	}
	return nil, true
}
func (director *Director) addTeamLead(teamLead TeamLead) (error,bool) {
	for _, val := range director.teamLeads{
		if val.EqualTo(teamLead){
			return fmt.Errorf("%v already exist", teamLead.nameOfTeam),false
		}
	}
	director.teamLeads = append(director.teamLeads, teamLead)
	return nil, true
}

type TeamLead struct {
	nameOfTeam string
	task task2.Task
	programmers []Programmer

}


func (teamLead TeamLead) EqualTo(tLead TeamLead)bool{
	res := teamLead.nameOfTeam == tLead.nameOfTeam
	return res

}
type Programmer struct {
	FullName string
}